#!/usr/bin/env python
# -*- coding: utf-8 -*-
import getopt
import os
import sys

from core import app, db
from routes import *


DEFAULT_HOST = '0.0.0.0'
DEFAULT_PORT = 5461


class Usage(Exception):
    def __init__(self, msg):
        self.msg = msg


def print_help(options, args):
    # TODO print help code
    pass


def work(options, args):
    host = DEFAULT_HOST
    port = DEFAULT_PORT
    if len(args) > 0:
        arg = args.pop()
        if ':' in arg:
            host, port_str = arg.split(':')
            port = int(port_str)
        else:
            port = int(arg)

    # Load configuration
    config_filename = options.get('-c') or options.get('--config',
                                                       'standard_config.py')
    app.config.from_pyfile(config_filename)

    # Create user settings directory if doesn't exist
    settings_dir_path = app.config.get('USER_SETTINGS_DIR_PATH')
    if settings_dir_path and not os.path.exists(settings_dir_path):
        os.makedirs(settings_dir_path)

    # Create database just once
    db.create_all()
    db.session.commit()

    ssl_context = (app.config.get('SSL_CERTIFICATE_PATH'),
                   app.config.get('SSL_PRIVATE_KEY_PATH'))
    app.run(host=host, port=port, ssl_context=ssl_context)


def main(argv=None):
    if argv is None:
        argv = sys.argv

    try:
        try:
            options, args = getopt.getopt(argv[1:],
                                          'hc:',
                                          ['help', 'config='])
            options = dict(options)
        except getopt.error, msg:
            raise Usage(msg)

        if '-h' in options or '--help' in options:
            print_help(options, args)
        else:
            work(options, args)

    except Usage, err:
        print >>sys.stderr, err.msg
        print >>sys.stderr, 'for help use --help'
        return 2


if __name__ == '__main__':
    sys.exit(main())
