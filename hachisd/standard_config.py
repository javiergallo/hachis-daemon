# -*- coding: utf-8 -*-
import os

DEBUG = False
TESTING = True
USER_SETTINGS_DIR_PATH = os.path.realpath(os.path.expanduser('~/.hachisd/'))
SQLALCHEMY_DATABASE_URI = 'sqlite:///{dir_path}/hachisd.db'.format(
    dir_path=USER_SETTINGS_DIR_PATH
)
REMOTE_ADDR_WHITELIST = ['127.*.*.*', '10.*.*.*', '192.168.*.*']

SSL_PRIVATE_KEY_PATH = '{dir_path}/hachisd.key'.format(
    dir_path=USER_SETTINGS_DIR_PATH
)
SSL_CERTIFICATE_PATH = '{dir_path}/hachisd.crt'.format(
    dir_path=USER_SETTINGS_DIR_PATH
)
