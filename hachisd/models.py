import pipes
import string
import subprocess

from flask import url_for
from werkzeug.security import generate_password_hash, check_password_hash

from core import db


class User(db.Model):
    username = db.Column(db.String(128), primary_key=True)
    password_hash = db.Column(db.String(128))

    def __init__(self, password=None, **kwargs):
        super(User, self).__init__(**kwargs)
        if password is not None:
            self.set_password(password)

    def __repr__(self):
        return '<{} {}>'.format(type(self).__name__, self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def password_is_valid(self, password):
        return check_password_hash(self.password_hash, password)


class Process(db.Model):
    slug = db.Column(db.String(128), primary_key=True)
    command = db.Column(db.String(256))

    def __init__(self, slug, command):
        self.slug = slug
        self.command = command

    def __repr__(self):
        return '<Process {0}: "{1}">'.format(self.slug, self.command)

    def run(self, kwargs={}):
        missing_args = self.command_args - set(kwargs.keys())
        if missing_args:
            quoted_missing_args = {'"{}"'.format(arg) for arg in missing_args}
            raise ValueError(
                'Arguments missing: {}.'.format(', '.join(quoted_missing_args))
            )

        safe_kwargs = {key: pipes.quote(kwargs[key]) for key in kwargs}
        command = self.command.format(**safe_kwargs)
        process = subprocess.Popen(command,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE,
                                   shell=True)

        return process.communicate()

    @property
    def execution_url(self):
        return url_for('get_process_done', slug=self.slug, _external=True)

    @property
    def command_args(self):
        formatter = string.Formatter()
        return {i[1] for i in formatter.parse(self.command) if i[1]}
