from flask import abort, json, request, Response
from fnmatch import fnmatch
from functools import wraps

from core import app
from models import Process, User


LOGIN_REQUIRED_RESPONSE = Response(
    'Login required',
    401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'}
)


def auth_is_valid(username, password):
    """Checks if a username / password combination is valid."""
    user = User.query.filter_by(username=username).first()
    return user is not None and user.password_is_valid(password)


def auth_required(func):
    @wraps(func)
    def decorated_func(*args, **kwargs):
        auth = request.authorization
        if not auth or not auth_is_valid(auth.username.decode('utf-8'),
                                         auth.password.decode('utf-8')):
            response = LOGIN_REQUIRED_RESPONSE
        else:
            response = func(*args, **kwargs)
        return response
    return decorated_func


@app.before_request
def limit_remote_addr():
    remote_addr_whitelist = app.config.get('REMOTE_ADDR_WHITELIST', [])
    must_abort = True
    for allowed_remote_addr_pattern in remote_addr_whitelist:
        if fnmatch(request.remote_addr, allowed_remote_addr_pattern):
            must_abort = False
            break

    if must_abort:
        abort(403)


@app.route('/')
@auth_required
def get_index():
    property_names = ['slug', 'execution_url', 'command_args']
    processes_data = []
    for process in Process.query.all():
        property_values = []
        for property_name in property_names:
            property_value = getattr(process, property_name)

            # Turn sets into lists since sets are not JSON serializable
            if isinstance(property_value, set):
                property_value = list(property_value)

            property_values.append(property_value)
        processes_data.append(property_values)

    return json.jsonify({
        'properties': property_names,
        'processes': processes_data,
        'error': ''
    })


@app.route('/<slug>/done')
@auth_required
def get_process_done(slug):
    process = Process.query.filter_by(slug=slug).first_or_404()
    kwargs = {key: request.args[key] for key in request.args}
    try:
        output, error = process.run(kwargs)
    except ValueError as error:
        abort(400, error)

    return json.jsonify({
        'output': output,
        'error': error
    })
