import os
from setuptools import setup


def open_file(filename):
    return open(os.path.join(os.path.dirname(__file__), filename))


requirements = [
    line.strip()
    for line in open_file('requirements.txt').readlines()
]

setup(
    name='hachis-daemon',
    version='0.4.1',
    author='Javier Gallo',
    author_email='javier_rooster@yahoo.com.ar',
    description='Hachis Daemon',
    url='https://bitbucket.org/javiergallo/hachis-daemon',
    packages=['hachisd'],
    license='MIT',
    long_description=open_file('README.md').read(),
    install_requires=requirements
)
